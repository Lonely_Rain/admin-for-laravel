## 管理员模块

> 引入组件

* 依赖模块
  + `Passport` 认证，也可以自己重新写认证
  
* `Laravel` 内部添加文件
  + 需要把 `config\error.php` 添加到 `laravel\config` 目录
  + 需要把 `Helpers` 目录添加到 `laravel` 下的 `app` 目录

* `Laravel` 内部更新文件
  + `config\app.php` 添加服务提供者 `Rain\Admin\AdminServiceProvider::class`
  + `app\Http\Kernel.php` 添加中间件 `'access' => \Rain\Admin\Http\Middleware\CheckAccess::class`
  + `App\Providers\AuthServiceProvider` 类添加 
    ```
        // 加载路由
        Admin::routes();

        // 加载权限控制
        AccessService::getInstance() -> init();
    ```
  
 > 路由
 
 * 路由前缀：`auth`
 
 * 路由详情：`Routes\admin.php`
 
 > 认证方式
 
 * 拒绝条件
   + 路由定义，未定义名称
   + 路由格式不正确
   
 * 格式：路由名称 + 请求方式
 
 * 角色 `id` 为 `1` 时，跳出认证
 
 > 数据库迁移
 
 * `Database\migrations`
