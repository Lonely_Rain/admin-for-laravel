<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/16
 * Time: 18:17
 */

namespace Rain\Admin;

use Illuminate\Support\Facades\Route;

class Admin
{
    /**
     * Binds the Passport routes into the controller.
     *
     * @return void
     */
    public function routes(){
        Route::prefix('auth')
        ->namespace('\Rain\Admin\Http\Controllers')
        ->group(__DIR__ . DIRECTORY_SEPARATOR . '/Routes/admin.php');
    }
}
