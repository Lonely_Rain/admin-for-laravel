<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/8/30
 * Time: 11:43
 */

if(!function_exists('echoArr')) {
    /**
     * 接口返回格式
     *
     * @param $code
     * @param string $msg
     * @param array $data
     * @return array
     */
    function echoArr($code, $msg = '', $data = []){
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    };
}

if(!function_exists('getErrorMessage')) {
    /**
     * 错误 提示
     *
     * @param $code
     * @param $config
     * @param string $type
     * @return mixed
     */
    function getErrorMessage($code, $config, $type = 'admin'){
        // 是否开启错误提示
        if(!$config['config']['error_prompt']) return $config['common'][400];

        // 错误码是否存在
        if(!isset($config[$type][$code])) return $config['common'][401];

        // 具体错误信息
        return $config[$type][$code];
    };
}

if(!function_exists('arrConversion')){
    /**
     * 数组处理
     *
     * @param $list
     * @param string $unique
     * @return mixed
     */
    function arrConversion($list, $unique = 'id')
    {
        return array_reduce($list, function ($result, $item) use ($unique) {
            $result[$item[$unique]] = $item;

            return $result;
        }, []);
    }
}

