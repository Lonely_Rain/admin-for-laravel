<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users', function (Blueprint $table) {
            // 表引擎
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // 表结构
            $table->increments('admin_id');
            $table->string('head_pic', 100) -> comment('头像') -> default('');
            $table->string('account', 50) -> comment('账户') -> unique() -> default('');
            $table->string('password', 255) -> comment('密码') -> default('');
            $table->char('mobile', 11) -> comment('手机号') -> unique() -> default('');
            $table->integer('login_frequency') -> comment('登录次数') -> default(0);
            $table->string('last_login_ip', 20) -> comment('最后一次登录ip') -> default('');
            $table->tinyInteger('status') -> comment('状态') -> default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_users');
    }
}
