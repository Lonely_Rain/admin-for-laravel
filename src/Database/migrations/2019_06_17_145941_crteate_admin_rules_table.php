<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrteateAdminRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_rules', function (Blueprint $table) {
            // 表引擎
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // 表结构
            $table->increments('rule_id');
            $table->string('name', 20) -> comment('权限名称') -> default('');
            $table->string('method', 20) -> comment('请求方式') -> default('');
            $table->string('value', 50) -> comment('权限值') -> default('');
            $table->integer('class_id') -> comment('权限分类id') -> default(0);
            $table->tinyInteger('status') -> comment('状态') -> default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_rules');
    }
}
