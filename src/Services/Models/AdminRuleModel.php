<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/11
 * Time: 10:48
 */

namespace Rain\Admin\Services\Models;


use Rain\Admin\Models\AdminClass;
use Rain\Admin\Models\AdminRule;

class AdminRuleModel extends Base
{
    static protected $instance;

    protected $rely = [
        'AdminRuleModel' => AdminRule::class,
        'AdminClassModel' => AdminClass::class
    ];

    /**
     * 权限列表
     *
     * @param $where
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function getList($where)
    {
        $this->isPage = true;

        $query = $this->getClassName('AdminRuleModel')::select(['rule_id as id', 'name', 'value', 'method', 'status', 'class_id']);

        if ($where) {
            if (isset($where['ruleName']) && $where['ruleName']) {
                $query->where('name', 'like', "%{$where['ruleName']}%");
            }

            if (isset($where['classId']) && $where['classId']) {
                $query->where('class_id', $where['classId']);
            }

            if (isset($where['method']) && $where['method']) {
                $query->where('method', $where['method']);
            }
        }

        return $this->all($query);
    }

    /**
     * 单条权限
     *
     * @param $id
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function getFind($id)
    {
        $query = $this->getClassName('AdminRuleModel')::where('rule_id', $id)->select(['rule_id as id', 'name', 'value', 'method', 'class_id']);

        return $this->find($query);
    }

    /**
     * 权限分类列表
     */
    public function getRuleClassList()
    {
        $this->isPage = false;

        $query = $this->getClassName('AdminClassModel')::select(['class_id as id', 'name']);

        return $this->all($query);
    }

    /**
     * 同一分类下权限是否存在
     *
     * @param $name
     * @param $classId
     * @param int $ruleId
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function isRouteRepeat($name, $classId, $ruleId = 0)
    {
        $query = $this->getClassName('AdminRuleModel')::where('name', $name)->where('class_id', $classId);

        if ($ruleId) $query->where('rule_id', '<>', $ruleId);

        $this->field = ['rule_id'];

        return $this->value($query);
    }

    /**
     *
     */
    public function isBindRole($id)
    {
        $query = $this->getClassName('AdminRuleModel')::where('rule_id', $id);

        if (!$rule = $this->find($query)) return [];

        return $rule->roles->pluck('role_id');
    }

    /**
     * 添加权限
     *
     * @param $data
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function addRule($data)
    {
        $this->add($this->getClassName('AdminRuleModel'), $data);
    }

    /**
     * 更新权限
     *
     * @param $id
     * @param $data
     * @throws \League\Flysystem\Exception
     */
    public function editRule($id, $data)
    {
        $query = $this->getClassName('AdminRuleModel')::where('rule_id', $id);

        $this->update($query, $data);
    }

    /**
     * 删除权限
     *
     * @param $id
     * @throws \League\Flysystem\Exception
     */
    public function delRule($id)
    {
        $query = $this->getClassName('AdminRuleModel')::where('rule_id', $id);

        $this->del($query);
    }

    /**
     * 获取模型处理类
     *
     * @param $id
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    protected function getModelClass($id){
        return $this -> getClassName('AdminRuleModel')::where('rule_id', $id);
    }
}
