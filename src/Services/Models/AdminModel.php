<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/4
 * Time: 16:23
 */

namespace Rain\Admin\Services\Models;


use Rain\Admin\Models\AdminRole;
use Rain\Admin\Models\AdminUser;

class AdminModel extends Base
{
    static protected $instance;

    protected $rely = [
        'AdminUserModel' => AdminUser::class,
        'AdminRoleModel' => AdminRole::class,
    ];

    /**
     * 管理员列表
     *
     * @param $where
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function adminList($where){
        $this -> isPage = true;

        $query = $this -> getClassName('AdminUserModel')::select(['admin_id as id', 'account', 'mobile', 'status', 'created_at']);

        if ($where) {
            if (isset($where['adminName']) && $where['adminName']) {
                $query->where('account', 'like', "%{$where['adminName']}%");
            }

            if (isset($where['mobile']) && $where['mobile']) {
                $query->where('mobile', 'like', "%{$where['mobile']}%");
            }
        }

        return $this -> all($query);
    }

    public function adminFind($id){
        $query = $this -> getClassName('AdminUserModel')::where('admin_id', $id) -> select(['admin_id', 'account', 'mobile', 'status']);

        // 管理员
        $admin = $this -> find($query);
        if(!$admin) return [];

        // 管理员所拥有的角色
        $admin['role'] = $admin -> roles() -> pluck('role_id') -> toArray();

        return $admin -> toArray();
    }

    /**
     * 角色列表
     *
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function roleList(){
        $query = $this -> getClassName('AdminRoleModel')::select(['role_id as id', 'name']);

        return $this -> all($query);
    }

    /**
     * 角色 id
     *
     * @param $ids
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function getRoleIds($ids){
        $query = $this -> getClassName('AdminRoleModel')::whereIn('role_id', $ids);

        $this -> field = ['role_id'];

        return $this -> pluck($query) -> toArray();
    }

    /**
     * 添加用户
     *
     * @param $data
     * @param $roleIds
     * @throws \League\Flysystem\Exception
     */
    public function addUser($data, $roleIds){
        // 添加用户
        $user = $this -> add($this -> getClassName('AdminUserModel'), $data);

        // 更新用户绑定的角色
        $this -> updateRole($user, $roleIds);
    }

    /**
     * 更新用户
     *
     * @param $id
     * @param $data
     * @param $roleIds
     * @throws \League\Flysystem\Exception
     */
    public function editUser($id, $data, $roleIds){
        // 更新用户
        $query = $this -> getClassName('AdminUserModel')::where('admin_id', $id);

        $user = $this -> update($query, $data);
        if(!$user) return;

        // 更新用户绑定的角色，不能更新超级管理员的角色
        if(1 != $id) $this -> updateRole($user, $roleIds);
    }

    /**
     * 更新用户绑定的角色
     *
     * @param $user
     * @param $roleIds
     */
    private function updateRole($user, $roleIds){
        $user -> roles() -> sync($roleIds);
    }

    /**
     * 删除用户
     *
     * @param $id
     * @throws \League\Flysystem\Exception
     */
    public function delUser($id){
        // 获取用户
        $query = $this -> getClassName('AdminUserModel')::where('admin_id', $id);
        $admin = $this -> find($query);

        if($admin) {
            // 删除用户绑定的角色
            $admin -> roles() -> detach();

            // 删除用户
            $admin -> delete();
        }
    }

    /**
     * 获取模型处理类
     *
     * @param $id
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    protected function getModelClass($id){
        return $this -> getClassName('AdminUserModel')::where('admin_id', $id);
    }
}
