<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/10
 * Time: 17:51
 */

namespace Rain\Admin\Services\Models;


use Rain\Admin\Models\AdminClass;
use Rain\Admin\Models\AdminRole;
use Rain\Admin\Models\AdminRule;

class AdminRoleModel extends Base
{
    static protected $instance;

    protected $rely = [
        'AdminRoleModel' => AdminRole::class,
        'AdminRuleModel' => AdminRule::class,
        'AdminClassModel' => AdminClass::class,
    ];

    /**
     * 角色列表
     *
     * @param $where
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function roleList($where){
        $this -> isPage = true;

        $query = $this -> getClassName('AdminRoleModel')::select(['role_id as id', 'name', 'status', 'description', 'created_at']) -> where('role_id', '<>', 1);

        if($where) {
            $query -> where('name', 'like', "%{$where['roleName']}%");
        }

        return $this -> all($query);
    }

    /**
     * 角色信息
     *
     * @param $id
     * @return array
     * @throws \League\Flysystem\Exception
     */
    public function roleFind($id){
        $query = $this -> getClassName('AdminRoleModel')::where('role_id', $id) -> select(['role_id', 'name', 'description']);

        // 角色
        $role = $this -> find($query);
        if(!$role) return [];

        // 角色所拥有的权限
        $role['rule'] = $role -> rules() -> pluck('rule_id') -> toArray();

        return $role -> toArray();
    }

    /**
     * 权限列表
     *
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function ruleList(){
        $query = $this -> getClassName('AdminRuleModel')::select(['rule_id as id', 'name', 'class_id']);

        return $this -> all($query);
    }

    /**
     * 权限 id
     *
     * @param $ids
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function getRuleIds($ids){
        $query = $this -> getClassName('AdminRuleModel')::whereIn('rule_id', $ids);

        $this -> field = ['rule_id'];

        return $this -> pluck($query) -> toArray();
    }

    /**
     * 角色是否被用户绑定
     *
     * @param $id
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function isBindUser($id){
        $query = $this -> getClassName('AdminRoleModel')::where('role_id', $id);

        $role = $this -> find($query);

        return $role -> users -> pluck('admin_id') -> toArray();
    }

    /**
     * 权限分类列表
     *
     * @param $classIds
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function classList($classIds){
        $query = $this -> getClassName('AdminClassModel')::whereIn('class_id', $classIds) -> select(['class_id', 'name']);

        return $this -> all($query);
    }

    /**
     * 添加角色
     *
     * @param $data
     * @param $ruleIds
     * @throws \League\Flysystem\Exception
     */
    public function addRole($data, $ruleIds){
        // 添加角色
        $role = $this -> add($this -> getClassName('AdminRoleModel'), $data);

        // 更新角色绑定的权限
        $this -> updateRule($role, $ruleIds);
    }

    /**
     * 更新用户
     *
     * @param $id
     * @param $data
     * @param $ruleIds
     * @throws \League\Flysystem\Exception
     */
    public function editRole($id, $data, $ruleIds){
        // 更新角色
        $query = $this -> getClassName('AdminRoleModel')::where('role_id', $id);
        $role = $this -> update($query, $data);

        // 更新角色绑定的权限，不能更新超级管理员的权限
        if(1 != $id) $this -> updateRule($role, $ruleIds);
    }

    /**
     * 更新角色绑定的权限
     *
     * @param $role
     * @param $ruleIds
     */
    private function updateRule($role, $ruleIds){
        $role -> rules() -> sync($ruleIds);
    }

    /**
     * 删除用户
     *
     * @param $id
     * @throws \League\Flysystem\Exception
     */
    public function delRole($id){
        // 获取角色
        $query = $this -> getClassName('AdminRoleModel')::where('role_id', $id);
        $role = $this -> find($query);

        if($role){
            // 删除角色绑定的权限
            $role -> rules() -> detach();

            // 删除角色
            $role -> delete();
        }
    }

    /**
     * 获取模型处理类
     *
     * @param $id
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    protected function getModelClass($id){
        return $this -> getClassName('AdminRoleModel')::where('role_id', $id);
    }
}
