<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/12
 * Time: 13:28
 */

namespace Rain\Admin\Services\Models;


use Rain\Admin\Models\AdminRule;

class AccessModel extends Base
{
    static protected $instance;

    protected $rely = [
        'AdminRuleModel' => AdminRule::class,
    ];

    /**
     * 权限列表
     *
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function ruleList(){
        $query = $this -> getClassName('AdminRuleModel')::where('status', 1) -> select(['value', 'method']);

        return $this -> all($query);
    }

    /**
     * 用户拥有的角色
     *
     * @param $user
     * @return mixed
     */
    public function userRoles($user){
        return $user -> roles() -> where('status', 1) -> with(['rules' => function ($query){
            $query -> where('status', 1);
        }]) -> get();
    }
}
