<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/12
 * Time: 15:54
 */

namespace Rain\Admin\Services\Models;


use Illuminate\Support\Facades\DB;
use Rain\Admin\Models\AdminClass;

class AdminClassModel extends Base
{
    static protected $instance;

    protected $rely = [
        'AdminClassModel' => AdminClass::class,
    ];

    /**
     * 分类列表
     *
     * @param $where
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function classList($where){
        $this -> isPage = true;

        $query = $this -> getClassName('AdminClassModel')::select(['class_id as id', 'name', 'description', 'created_at']);

        // 筛选条件
        if($where) {
            if(isset($where['className']) && $where['className']) {
                $query -> where('name', 'like', "%{$where['className']}%");
            }
        }

        return $this -> all($query);
    }

    /**
     * 分类信息
     *
     * @param $id
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function classFind($id){
        $query = $this -> getClassName('AdminClassModel')::where('class_id', $id) -> select(['class_id as id', 'name', 'description']);

        return $this -> find($query);
    }

    /**
     * 此分类是否被权限绑定
     *
     * @param $id
     * @return mixed
     * @throws \League\Flysystem\Exception
     */
    public function isBindRule($id){
        $query = $this -> getClassName('AdminClassModel')::where('class_id', $id) -> select(['class_id']);

        $result = $this -> find($query);

        return $result ? $result -> rules() -> pluck('rule_id') -> toArray() : [];
    }

    /**
     * 添加分类
     *
     * @param $data
     * @throws \League\Flysystem\Exception
     */
    public function addClass($data){
        $this -> add($this -> getClassName('AdminClassModel'), $data);
    }

    /**
     * 修改分类
     *
     * @param $id
     * @param $data
     * @throws \League\Flysystem\Exception
     */
    public function editClass($id, $data){
        $query = $this -> getClassName('AdminClassModel')::where('class_id', $id);

        $this -> update($query, $data);
    }

    /**
     * 删除分类
     *
     * @param $id
     * @throws \League\Flysystem\Exception
     */
    public function delClass($id){
        $query = $this -> getClassName('AdminClassModel')::where('class_id', $id);

        $this -> del($query);
    }
}
