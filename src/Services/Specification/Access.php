<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/8/29
 * Time: 14:27
 */

namespace Rain\Admin\Services\Specification;


interface Access
{
    public function login($username, $password);

    public function signOut();

    public function resetToken($refreshToken);
}
