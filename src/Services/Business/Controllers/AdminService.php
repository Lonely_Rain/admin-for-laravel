<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/4
 * Time: 16:14
 */

namespace Rain\Admin\Services\Business\Controllers;


use Illuminate\Support\Facades\Auth;
use Rain\Admin\Services\Models\AdminModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminService extends Base
{
    static protected $instance;

    /**
     * 依赖包管理
     *
     * @var array
     */
    protected $rely = [
        'mAdmin' => AdminModel::class,
    ];

    /**
     * 获取 用户列表
     *
     * @return mixed
     */
    public function getList(){
        $list = $this -> mAdmin -> adminList($this -> clearWhere());

        return $list;
    }

    /**
     * 获取 权限列表
     *
     * @return mixed
     */
    public function getRoleList(){
        $list = $this -> mAdmin -> roleList();

        return $list;
    }

    /**
     * 获取 用户信息
     */
    public function find(){
        $id = $this -> data;

        $result = $this -> mAdmin -> adminFind($id);
        if(!$result) return $result;

        $result['id'] = $result['admin_id'];
        unset($result['admin_id']);

        return $result;
    }

    /**
     * 操作
     *
     * @param $id
     * @return int
     */
    public function operating($id = 0){
        $params = $this -> data;

        // 普通用户不可修改管理员的信息
        if(1 == $id && Auth::id() != $id) return echoArr(500, '超级管理员不可被修改');

        // 操作所需数据
        $data = [
            'account' => $params['username'],
            'mobile' => $params['mobile'],
        ];

        // 密码存在时，加密
        if(isset($params['password']) && trim($params['password'])) $data['password'] = Hash::make($params['password']);

        // 角色 id
        $roleIds = $this -> mAdmin -> getRoleIds(json_decode($params['role_ids'], true));

        DB::beginTRansaction();

        try {
            if(!$id) {
                $this -> mAdmin -> addUser($data, $roleIds);
            } else {
                $this -> mAdmin -> editUser($id, $data, $roleIds);
            }

            DB::commit();

            return 200;
        } catch (\Exception $exception) {
            DB::rollBack();

            return 800;
        }
    }

    /**
     * 删除用户
     */
    public function del(){
        $id = $this -> data;

        if(1 == $id || $id == Auth::id()) return 800;

        DB::beginTransaction();

        try {
            $this -> mAdmin -> delUser($id);

            DB::commit();

            return 200;
        } catch (\Exception $exception) {
            DB::rollBack();

            return 800;
        }
    }

    /**
     * 获取状态
     *
     * @param $data
     * @return array
     */
    protected function getStatus($data){
        return [
            'status' => $data['status']
        ];
    }

    /**
     * 获取模型处理类
     *
     * @return mixed
     */
    protected function getModelClass(){
        return $this -> mAdmin;
    }

    /**
     * 修改单个值逻辑处理
     *
     * @param $id       用户id
     * @param $data     用户修改数据
     * @return array
     */
    protected function handlerIndividualValue($id, $data){
        // 不允许修改超级管理员的状态
        if($id == 1) return echoArr(690);

        // 不允许修改自己的状态
        if($id == Auth::id()) return echoArr(691);

        return echoArr(200);
    }
}
