<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/8/29
 * Time: 13:59
 */

namespace Rain\Admin\Services\Business\Controllers;


use App\Http\Controllers\Controller;
use Rain\Admin\Services\Business\Common\Rely;
use Rain\Admin\Services\Business\Common\Share\SharedService;
use Rain\Admin\Services\Business\Common\SingleCase;

class Base extends Controller
{
    use SingleCase, Rely, SharedService;
}
