<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/12
 * Time: 15:23
 */

namespace Rain\Admin\Services\Business\Controllers;


use Rain\Admin\Services\Models\AdminClassModel;

class AdminClassService extends Base
{
    static protected $instance;

    protected $rely = [
        'mAdminClass' => AdminClassModel::class,
    ];

    /**
     * 分类列表
     *
     * @return mixed
     */
    public function getList(){
        return $this -> mAdminClass -> classList($this -> clearWhere());
    }

    /**
     * 单条数据
     */
    public function getClass(){
        $id = $this -> data;

        return $this -> mAdminClass -> classFind($id);
    }

    /**
     * 操作
     *
     * @param int $id
     * @return array
     */
    public function operating($id = 0){
        $params = $this -> data;

        // 操作所需数据
        $data = [
            'name' => $params['name'],
            'description' => $params['description']
        ];

        try {
            if(!$id) {
                $this -> mAdminClass -> addClass($data);
            } else {
                $this -> mAdminClass -> editClass($id, $data);
            }

            return echoArr(200);
        } catch (\Exception $exception) {
            return echoArr(800);
        }
    }

    /**
     * 删除分类
     */
    public function del(){
        $id = $this -> data;

        // 判断是否有权限已绑定此分类
        if($this -> mAdminClass -> isBindRule($id)) return echoArr(720);

        try {
            $this -> mAdminClass -> delClass($id);

            return echoArr(200, '删除成功');
        } catch (\Exception $exception) {
            return echoArr(800);
        }
    }
}
