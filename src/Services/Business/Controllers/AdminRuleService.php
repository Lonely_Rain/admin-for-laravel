<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/11
 * Time: 10:44
 */

namespace Rain\Admin\Services\Business\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Rain\Admin\Services\Models\AdminRuleModel;

class AdminRuleService extends Base
{
    static protected $instance;

    protected $rely = [
        'mRule' => AdminRuleModel::class,
    ];

    /**
     * 权限列表
     *
     * @return mixed
     */
    public function ruleList()
    {
        $list = $this->mRule->getList($this->clearWhere());

        foreach ($list as $k => $v) {
            $list[$k]['className'] = $v -> classInfo() -> value('name');

            unset($list[$k]['class_id']);
        }

        return $list;
    }

    /**
     * 获取权限信息
     *
     * @return mixed
     */
    public function getRule()
    {
        $id = $this->data;

        return $this->mRule->getFind($id);
    }

    /**
     * 路由列表
     *
     * @return array|mixed
     */
    public function routeList()
    {
        // 黑名单的路由前缀
        list($blackPrefix, $blackRoute) = [['oauth'], ['login', 'refreshToken', 'signOut']];

        // 路由列表
        list($routes, $path) = [app('router')->getRoutes(), []];
        foreach ($routes as $route) {
            $action = $route->getAction();

            // 黑名单内的路由前缀不参与权限管理
            if (isset($action['prefix']) && in_array($action['prefix'], $blackPrefix)) continue;

            // 黑名单内的路由不参与权限管理
            if (isset($action['as']) && in_array($action['as'], $blackRoute)) continue;

            // 未定义路由别名不参与权限管理
            if (!isset($action['as'])) continue;

            $path[] = [
                'method' => $route->methods[0],
                'value' => $action['as']
            ];
        }

        return $path;
    }

    /**
     * 权限分类列表
     */
    public function getRuleClass()
    {
        return $this->mRule->getRuleClassList();
    }

    /**
     * 权限请求类型列表
     */
    public function getMethods()
    {
        return ['GET', 'POST', 'PUT', 'DELETE'];
    }

    /**
     * 操作数据
     *
     * @param int $id
     * @return array
     */
    public function operating($id = 0)
    {
        $params = $this->data;

        $method = strtoupper($params['method']);

        // 请求类型是否正确
        if (!in_array($method, $this->getMethods())) return echoArr(711);

        // 路由名称是否在同一分类下重复
        $ruleId = $this->mRule->isRouteRepeat($params['name'], $params['class_id'], $id);
        if ($ruleId) return echoArr(710);

        // 需操作的数据
        $data = [
            'name' => $params['name'],
            'value' => $params['route'],
            'method' => $method,
            'class_id' => $params['class_id']
        ];

        // 操作
        try {
            if (!$id) {
                $this->mRule->addRule($data);
            } else {
                $this->mRule->editRule($id, $data);
            }

            return echoArr(200);
        } catch (\Exception $exception) {
            return echoArr(800);
        }
    }

    /**
     * 删除
     */
    public function del()
    {
        $id = $this->data;

        // 是否有角色绑定此权限
        if ($this->mRule->isBindRole($id)) return echoArr(714);

        try {
            $this->mRule->delRule($id);

            return echoArr(200, '删除成功');
        } catch (\Exception $exception) {
            return echoArr(800);
        }
    }

    /**
     * 获取模型处理类
     *
     * @return mixed
     */
    protected function getModelClass(){
        return $this -> mRule;
    }
}
