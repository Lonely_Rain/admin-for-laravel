<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/4
 * Time: 17:44
 */

namespace Rain\Admin\Services\Business\Common;


use League\Flysystem\Exception;

trait Rely
{
    // 依赖包
    protected $rely;

    /**
     * 依赖管理
     *
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        // 调用静态方法 getInstance
        $this -> $name = call_user_func_array([$this -> getClassName($name), 'getInstance'], []);

        return $this -> $name;
    }

    /**
     * 获取 class 的类名
     *
     * @param $name
     * @return mixed
     * @throws Exception
     */
    protected function getClassName($name){
        // 异常处理
        if(!isset($this -> rely[$name])) throw new Exception("Dependency $name does not exist.");

        return $this -> rely[$name];
    }
}
