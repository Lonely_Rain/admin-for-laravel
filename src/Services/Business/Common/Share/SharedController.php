<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/11/13
 * Time: 15:30
 */

namespace Rain\Admin\Services\Business\Common\Share;


use Rain\Admin\Http\Requests\Common;

trait SharedController
{
    /**
     * 修改状态
     *
     * @param Common $request       控制器的验证类
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Common $request, $id){
        $class = $this -> getServiceClass();

        $data = $request -> only(['status']);

        $result = $class::getInstance($data) -> editIndividualValue($id, 1);

        return $this -> returnData($result['code'], $request['msg'], $result['data']);
    }
}
