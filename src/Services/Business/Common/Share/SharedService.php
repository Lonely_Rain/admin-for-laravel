<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/11/13
 * Time: 15:44
 */

namespace Rain\Admin\Services\Business\Common\Share;


trait SharedService
{
    /**
     * 清除默认条件
     *
     * @return mixed
     */
    protected function clearWhere(){
        $data = $this -> data;

        unset($data['page']);

        return $data;
    }

    /**
     * 不确定数据
     *
     * @param mixed ...$field
     * @return mixed
     */
    protected function variableData(...$field){
        $params = $this -> data;

        return array_reduce($field, function ($result, $item) use($params){
            if(isset($params[$item[0]])) $result[$item[1]] = $params[$item[0]];

            return $result;
        }, []);
    }

    /**
     * 修改单个值
     *
     * 例如：状态，排序等
     *
     * @param $id       用户id
     * @param $mode     模式
     * @return array
     */
    public function editIndividualValue($id, $mode){
        list($data, $class) = [$this -> data, get_called_class()];

        // 逻辑处理
        if(method_exists($class, 'handlerIndividualValue')) {
            $result = $this -> handlerIndividualValue($id, $data);

            if(200 != $result['code']) return $result;
        }

        // 数据结构是否需要调整
        switch ($mode) {
            // 用户状态
            case 1:
                if(method_exists($class, 'getStatus')) $data = $this -> getStatus($data);
                break;
        }

        // 更新数据
        try {
            $this -> getModelClass() -> updateIndividualValue($id, $data);

            return echoArr(200);
        } catch (\Exception $exception) {
            return echoArr(800);
        }
    }
}
