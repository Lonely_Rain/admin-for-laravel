<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/4
 * Time: 17:18
 */

namespace Rain\Admin\Services\Business\Common;


trait SingleCase
{
    protected $data;

    private function __clone(){}

    private function __construct($data)
    {
        $this -> data = $data;
    }

    static public function getInstance($data = []){
        $class = get_called_class();

        if(!$class::$instance instanceof $class) $class::$instance = new $class($data);

        return $class::$instance;
    }
}
