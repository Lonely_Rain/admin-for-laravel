<?php

namespace Rain\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminClass extends Model
{
    //
    use SoftDeletes;

    protected $table = 'admin_class';

    protected $primaryKey = 'class_id';

    protected $fillable = ['name', 'description'];

    public function rules(){
        return $this -> hasMany(AdminRule::class, 'class_id');
    }
}
