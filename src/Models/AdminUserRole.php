<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/8/6
 * Time: 16:20
 */

namespace Rain\Admin\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;

class AdminUserRole extends Pivot
{
    protected $primaryKey = 'admin_role_id';
}
