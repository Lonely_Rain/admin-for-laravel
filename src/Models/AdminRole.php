<?php

namespace Rain\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRole extends Model
{
    //
    use SoftDeletes;

    protected $primaryKey = 'role_id';

    protected $fillable = ['name', 'description', 'status'];

    public function rules(){
        return $this -> belongsToMany(AdminRule::class, 'admin_role_rules', 'role_id', 'rule_id') -> using(AdminRoleRule::class) -> withTimestamps() -> select('admin_rules.rule_id', 'admin_rules.name', 'admin_rules.value');
    }

    public function users(){
        return $this -> belongsToMany(AdminUser::class, 'admin_user_roles', 'role_id', 'admin_id') -> using(AdminRoleRule::class) -> withTimestamps();
    }
}
