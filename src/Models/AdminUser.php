<?php

namespace Rain\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class AdminUser extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    protected $primaryKey = 'admin_id';

    protected $fillable = ['account', 'password', 'mobile', 'status'];

    public function roles(){
        return $this -> belongsToMany(AdminRole::class, 'admin_user_roles', 'admin_id', 'role_id') -> withTimestamps() -> select('admin_roles.role_id', 'admin_roles.name', 'admin_roles.description');
    }

    public function findForPassport($username){
        return $this->where(function ($query) use ($username){
            $query ->orWhere('mobile', $username)->orWhere('account', $username);
        })->where('status', 1)->first();
    }
}
