<?php

namespace Rain\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRule extends Model
{
    use SoftDeletes;

    //
    protected $primaryKey = 'rule_id';

    protected $fillable = ['name', 'value', 'method', 'class_id', 'status'];

    public function roles(){
        return $this -> belongsToMany(AdminRole::class, 'admin_role_rules', 'rule_id', 'role_id') -> using(AdminRoleRule::class) -> withTimestamps();
    }

    public function classInfo(){
        return $this -> hasOne(AdminClass::class, 'class_id', 'class_id');
    }
}
