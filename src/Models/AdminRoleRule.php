<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/8/6
 * Time: 13:19
 */

namespace Rain\Admin\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;

class AdminRoleRule extends Pivot
{
    protected $primaryKey = 'role_rule_id';
}
