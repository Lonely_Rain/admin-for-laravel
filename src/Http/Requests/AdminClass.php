<?php

namespace Rain\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminClass extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this -> route('admin_class');

        return [
            'name' => ['bail', 'required', 'between:1,10', 'unique:admin_class,name,' . $id . ',class_id'],
            'description' => ['bail', 'required', 'between:1,100']
        ];
    }
}
