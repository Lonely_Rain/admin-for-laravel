<?php

namespace Rain\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Rain\Admin\Rules\Mobile;
use Rain\Admin\Rules\Pwd;

class Access extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = $this -> route() -> getAction()['as'];

        switch ($routeName) {
            case 'login':
                $rules = [
                    'username' => ['bail', 'required', new Mobile],
                    'password' => ['bail', 'required', new Pwd],
                ];
                break;

            case 'refreshToken':
                $rules = [
                    'refreshToken' => ['bail', 'required'],
                ];
                break;

            default:
                $rules = [];
        }

        return $rules;
    }

    public function messages()
    {
        return [

        ];
    }
}
