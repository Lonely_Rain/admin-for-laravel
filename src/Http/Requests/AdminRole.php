<?php

namespace Rain\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this -> route('admin_role');

        return [
            //
            'name' => ['bail', 'required', 'between:2,20', 'unique:admin_roles,name,' . $id . ',role_id'],
            'description' => ['bail', 'between:1,100'],
            'rule_ids' => ['bail', 'required', 'json'],
        ];
    }
}
