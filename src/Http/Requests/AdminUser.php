<?php

namespace Rain\Admin\Http\Requests;

use Rain\Admin\Rules\Mobile;
use Rain\Admin\Rules\Pwd;
use Illuminate\Foundation\Http\FormRequest;

class AdminUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this -> route('admin_user');

        return $rule = [
            'username' => ['bail', 'required', 'between:5,15', "unique:admin_users,account," . $id . ',admin_id'],
            'password' => ['bail', $id ? 'sometimes' : 'required', new Pwd],
            'mobile' => ['bail', 'required', new Mobile, 'unique:admin_users,mobile,' . $id . ',admin_id'],
            'role_ids' => ['bail', 'required', 'json'],
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
