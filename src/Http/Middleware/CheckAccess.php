<?php

namespace Rain\Admin\Http\Middleware;

use Rain\Admin\Services\Business\Common\Unified;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;

class CheckAccess
{
    use Unified;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        // 用户状态是否启用
        if(!$user -> status) return $this -> returnData(615);

        // 超级管理员则跳过验证权限
        if($user -> roles -> contains('role_id', 1)) return $next($request);

        // 判断是否有权限
        $rule = Route::currentRouteName() . '_' . strtoupper($request -> getMethod());
        if(!Route::currentRouteName() || Gate::denies($rule)){
            return $this -> returnData($request -> ajax() ? 613 : 614);
        }

        return $next($request);
    }
}
