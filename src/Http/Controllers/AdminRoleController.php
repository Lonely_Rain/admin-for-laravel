<?php

namespace Rain\Admin\Http\Controllers;

use Rain\Admin\Http\Requests\AdminRole;
use Rain\Admin\Services\Business\Controllers\AdminRoleService;
use Illuminate\Http\Request;

class AdminRoleController extends Controller
{
    /**
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $list = AdminRoleService::getInstance($request -> only(['roleName'])) -> getList();

        return $this -> returnData(200, '请求成功', [
            'list' => $list
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        // 权限列表
        $role =AdminRoleService::getInstance() -> getRuleList();

        return $this -> returnData(200, '请求成功', [
            'rules' => $role
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminRole $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AdminRole $request)
    {
        //
        $params = $request -> only(['name', 'description', 'rule_ids']);

        $code = AdminRoleService::getInstance($params) -> operating();

        return $this -> returnData($code, 200 == $code ? '创建成功' : '');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $result = AdminRoleService::getInstance($id) -> find();

        return $this -> returnData(200, '请求成功', [
            'role' => $result
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return $this -> returnData(200, '请求成功', [
            'role' => AdminRoleService::getInstance($id) -> find(),
            'ruleList' => AdminRoleService::getInstance($id) -> getRuleList()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminRole $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AdminRole $request, $id)
    {
        //
        $params = $request -> only(['name', 'description', 'rule_ids']);

        $code = AdminRoleService::getInstance($params) -> operating($id);

        return $this -> returnData($code, 200 == $code ? '修改成功' : '');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $code = AdminRoleService::getInstance($id) -> del($id);

        return $this -> returnData($code, 200 == $code ? '删除成功' : '');
    }

    /**
     * Get ervice class name
     *
     * @return string
     */
    public function getServiceClass(){
        return AdminRoleService::class;
    }
}
