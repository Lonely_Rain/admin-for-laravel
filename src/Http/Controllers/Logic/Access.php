<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/8/8
 * Time: 11:06
 */

namespace Rain\Admin\Http\Controllers\Logic;

use Rain\Admin\Http\Requests\Access as vAccess;
use Rain\Admin\Services\Specification\Access as iAccess;

trait Access
{
    public function login(vAccess $request, iAccess $access){
        $params = $request -> only(['username', 'password']);

        $result = $access -> login($params['username'], $params['password']);

        return $this -> returnData($result['code'], 200 == $result['code'] ? '登录成功' : '', $result['data']);
    }

    public function signOut(iAccess $access){
        $result = $access -> signOut();

        return $this -> returnData($result['code'], 200 == $result['code'] ? '退出成功' : '', $result['data']);
    }

    public function refreshToken(vAccess $request, iAccess $access){
        $params = $request -> only(['refreshToken']);

        $result = $access -> resetToken($params['refreshToken']);

        return $this -> returnData($result['code'], 200 == $result['code'] ? '重置成功' : '', $result['data']);
    }
}
