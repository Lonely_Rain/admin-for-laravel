<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/11/16
 * Time: 10:40
 */

namespace Rain\Admin\Http\Controllers\Logic\Admin;


use Illuminate\Support\Facades\Auth;

trait User
{
    /**
     * 获取当前登录用户信息
     *
     * @return mixed
     */
    public function getUserInfo(){
        $user = Auth::user();

        return $this -> returnData(200, '请求成功', [
            'info' => [
                'head_pic' => $user['head_pic'],
                'account' => $user['account'],
                'mobile' => $user['mobile'],
            ]
        ]);
    }
}
