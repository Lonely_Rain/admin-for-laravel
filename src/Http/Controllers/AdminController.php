<?php

namespace Rain\Admin\Http\Controllers;

use Rain\Admin\Http\Controllers\Logic\Access;
use Rain\Admin\Http\Controllers\Logic\Admin\User;
use Rain\Admin\Services\Business\Controllers\AdminService;
use Illuminate\Http\Request;
use Rain\Admin\Http\Requests\AdminUser as vAdminUser;

class AdminController extends Controller
{
    use Access, User;

    /**
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $list = AdminService::getInstance($request -> only(['adminName', 'mobile'])) -> getList();

        return $this -> returnData(200, '请求成功', [
            'list' => $list
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        // 角色列表
        $role =AdminService::getInstance() -> getRoleList();

        return $this -> returnData(200, '请求成功', [
            'role' => $role
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param vAdminUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(vAdminUser $request)
    {
        //
        $params = $request -> only(['username', 'password', 'mobile', 'role_ids', 'status']);

        $code = AdminService::getInstance($params) -> operating();

        return $this -> returnData($code, 200 == $code ? '创建成功' : '');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $result = AdminService::getInstance($id) -> find();

        return $this -> returnData(200, '请求成功', [
            'admin' => $result
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return $this -> returnData(200, '请求成功', [
            'admin' => AdminService::getInstance($id) -> find(),
            'roleList' => AdminService::getInstance($id) -> getRoleList()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param vAdminUser $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(vAdminUser $request, $id)
    {
        //
        $params = $request -> only(['username', 'mobile', 'role_ids', 'password', 'status']);

        $code = AdminService::getInstance($params) -> operating($id);

        return $this -> returnData($code, 200 == $code ? '修改成功' : '');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $code = AdminService::getInstance($id) -> del($id);

        return $this -> returnData($code, 200 == $code ? '删除成功' : '');
    }

    /**
     * Get ervice class name
     *
     * @return string
     */
    public function getServiceClass(){
        return AdminService::class;
    }
}
