<?php

namespace Rain\Admin\Http\Controllers;

use Rain\Admin\Http\Requests\AdminClass;
use Rain\Admin\Services\Business\Controllers\AdminClassService;
use Illuminate\Http\Request;

class AdminClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        //
        $list = AdminClassService::getInstance($request -> only(['className'])) -> getList();

        return $this -> returnData(200, '请求成功', [
            'list' => $list
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        //
        return $this -> returnData(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminClass $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AdminClass $request)
    {
        //
        $params = $request -> only(['name', 'description']);

        $result = AdminClassService::getInstance($params) -> operating();

        return $this -> returnData($result['code'], $result['msg'], $result['data']);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        //
        $result = AdminClassService::getInstance($id) -> getClass();

        return $this -> returnData(200, '', [
            'class' => $result
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        //
        return $this -> returnData(200, '', [
            'class' => AdminClassService::getInstance($id) -> getClass()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminClass $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AdminClass $request, $id)
    {
        //
        $params = $request -> only(['name', 'description']);

        $result = AdminClassService::getInstance($params) -> operating($id);

        return $this -> returnData($result['code'], $result['msg'], $result['data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        //
        $result = AdminClassService::getInstance($id) -> del();

        return $this -> returnData($result['code'], $result['msg'], $result['data']);
    }

    /**
     * Get ervice class name
     *
     * @return string
     */
    public function getServiceClass(){
        return AdminClassService::class;
    }
}
