<?php

namespace Rain\Admin\Http\Controllers;

use Rain\Admin\Services\Business\Common\Unified;
use Rain\Admin\Services\Business\Common\Share\SharedController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Unified, SharedController;
}
