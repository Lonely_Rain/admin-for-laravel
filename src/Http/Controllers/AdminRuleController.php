<?php

namespace Rain\Admin\Http\Controllers;

use Rain\Admin\Http\Requests\AdminRule;
use Rain\Admin\Services\Business\Controllers\AdminRuleService;
use Illuminate\Http\Request;

class AdminRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        //
        $list = AdminRuleService::getInstance($request -> only(['ruleName', 'classId', 'method'])) -> ruleList();

        return $this -> returnData(200, '请求成功', [
            'list' => $list,
            'classList' => AdminRuleService::getInstance() -> getRuleClass(),
            'methods' => AdminRuleService::getInstance() -> getMethods()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $list = AdminRuleService::getInstance() -> routeList();
        $classList = AdminRuleService::getInstance() -> getRuleClass();
        $methods = AdminRuleService::getInstance() -> getMethods();

        return  $this -> returnData(200, '请求成功', [
            'rule' => $list,
            'classList' => $classList,
            'methods' => $methods
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminRule $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AdminRule $request)
    {
        //
        $params = $request -> only(['name', 'route', 'class_id', 'method']);

        $result = AdminRuleService::getInstance($params) -> operating();

        return $this -> returnData($result['code'], $result['msg'], $result['data']);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        //
        $result = AdminRuleService::getInstance($id) -> getRule();

        return $this -> returnData(200, '', [
            'rule' => $result
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        //
        return $this -> returnData(200, '', [
            'ruleList' => AdminRuleService::getInstance($id) -> routeList(),
            'classList' => AdminRuleService::getInstance() -> getRuleClass(),
            'methods' => AdminRuleService::getInstance() -> getMethods(),
            'rule' => AdminRuleService::getInstance($id) -> getRule(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminRule $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AdminRule $request, $id)
    {
        //
        $params = $request -> only(['name', 'route', 'class_id', 'method']);

        $result = AdminRuleService::getInstance($params) -> operating($id);

        return $this -> returnData($result['code'], $result['msg'], $result['data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $result = AdminRuleService::getInstance($id) -> del();

        return $this -> returnData($result['code'], $result['msg']);
    }

    /**
     * Get ervice class name
     *
     * @return string
     */
    public function getServiceClass(){
        return AdminRuleService::class;
    }
}
