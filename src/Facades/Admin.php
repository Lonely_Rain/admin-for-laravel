<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/16
 * Time: 18:21
 */

namespace Rain\Admin\Facades;


use Illuminate\Support\Facades\Facade;

class Admin extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'admin';
    }
}
