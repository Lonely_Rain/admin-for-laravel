<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/9/16
 * Time: 17:40
 */

namespace Rain\Admin;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Rain\Admin\Services\Business\Specific\AccessCard;
use Rain\Admin\Services\Specification\Access;

class AdminServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function boot(){
        // 后端 用户登录 处理所用类
        $this -> app -> bind('AccessHttp', function($app){
            return new Client();
        });

        // 数据迁移
        $this -> loadMigrationsFrom(__DIR__ . DIRECTORY_SEPARATOR . "Database" . DIRECTORY_SEPARATOR . "migrations");
    }

    public function register(){
        // 后端 用户 类
        $this->app->singleton('admin', function ($app) {
            return new Admin();
        });

        // 后端 用户 访问逻辑
        $this -> app -> bind(Access::class, function ($app){
            return new AccessCard();
        });
    }

    public function provides()
    {
        return ['admin'];
    }
}
