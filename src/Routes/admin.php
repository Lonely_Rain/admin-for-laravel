<?php

Route::post('login', 'AdminController@login') -> name('login');

// 是否已登录
Route::group(['middleware' => ['auth:admin']], function (){
    Route::post('refreshToken', 'AdminController@refreshToken') -> name('refreshToken');
    Route::delete('signOut', 'AdminController@signOut') -> name('signOut');

    // 验证是否有操作权限
    Route::group(['middleware' => ['access']], function (){
        Route::get('userInfo', 'AdminController@getUserInfo') -> name('admin-user.user_info');

        Route::resource('admin-user', 'AdminController');
        Route::put('admin-user/{id}/status', 'AdminController@status') -> name('admin-user.status');

        Route::resource('admin-rule', 'AdminRuleController');
        Route::put('admin-rule/{id}/status', 'AdminRuleController@status') -> name('admin-rule.status');

        Route::resource('admin-role', 'AdminRoleController');
        Route::put('admin-role/{id}/status', 'AdminRoleController@status') -> name('admin-role.status');

        Route::resource('admin-class', 'AdminClassController');
    });
});


